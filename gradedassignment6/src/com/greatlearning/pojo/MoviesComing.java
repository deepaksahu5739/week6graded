package com.greatlearning.pojo;

public class MoviesComing {

	private int id;
	private String title;
	private String releaseDate;
	private String contentRating;
	private int year;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getContentRating() {
		return contentRating;
	}

	public void setContentRating(String contentRating) {
		this.contentRating = contentRating;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return "MoviesComing [id=" + id + ", title=" + title + ", releaseDate=" + releaseDate + ", contentRating="
				+ contentRating + ", year=" + year + "]";
	}
}
