package week6asignment;

//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;

public class MainClass {

	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub

		FactoryPatternTypeMovie movieFactory = new FactoryPatternTypeMovie();
		
	      //get an object of Circle and call its draw method.
	     IMovies comingSoonMovies = movieFactory.getMovies("MOVIES_COMMING");

	      //call draw method of Circle
	     comingSoonMovies.display();
	      
	      IMovies theatricalMovies = movieFactory.getMovies("MOVIES_IN_THEATRES");
		
	      theatricalMovies.display();
	      
	    IMovies topRatedMoviesInIndia = movieFactory.getMovies("TOP_RATED_INDIA");
			
	    topRatedMoviesInIndia.display();
	      
	    IMovies topRatedMovies = movieFactory.getMovies("TOP_RATED_MOVIES");
			
	    topRatedMovies.display();
	    }
	}