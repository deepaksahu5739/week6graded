create database database3;
use database3;



create table Movies_Coming(ID int,
Title varchar(20),
Year int,
Category varchar(20));


create table Movies_inTheatres
 (
  ID                int,
  Title             varchar(20),
  Year              int,
  Category          varchar(20)
);

create table Top_RatedIndia
 (
  ID                int,
  Title             varchar(20),
  Year              int,
  Category          varchar(20)
);


create table Top_RatedMovies
 (
  ID                int,
  Title             varchar(20),
  Year              int,
  Category          varchar(20)
);


insert into Movies_Coming values(1,'beastt',2021,'Movies Coming');
insert into Movies_Coming values(2,'jaibhim',2021,'Movies Coming');
insert into Movies_coming values(3,'valimai',2022,'Movies Comming');

insert into Movies_inTheatres values(1,'Black Panther',2018,'Movies in Theatres');
insert into Movies_inTheatres values(2,'Grottmannen Dug',2018,'Movies in Theatres');
insert into Movies_inTheatres values(3,'Aiyaary',2018,'Movies in Theatres');


insert into Top_RatedIndia values(1,'Anand',1971,'Top Rated India');
insert into Top_RatedIndia values(2,'Dangal',2016,'Top Rated India');
insert into Top_RatedIndia values(3,'Drishyam',2013,'Top Rated India');

insert into Top_RatedMovies values(1,'Baazigar',1993,'Top Rated Movies');
insert into Top_RatedMovies values(2,'24',2016,'Top Rated Movies');
insert into Top_RatedMovies values(3,'Jodhaa Akbar',2008,'Top Rated Movies');


select*from Movies_Coming;
select*from Top_RatedIndia;
select*from Top_RatedMovies;
select*from Movies_inTheatres;